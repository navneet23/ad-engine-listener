package myntra_ad_engine.config;


import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import myntra_ad_engine.listener.Receiver;

@Configuration
public class RabbitConfig {
	

   private final static String defualtQueueName = "listener";
   private final static String defaultExchange = "SPE";
   
   @Autowired
   public static Environment env ;
   

   public static String queueName(){
	   return env.getProperty("spe.queueName", defualtQueueName);
   }
   
   public static String exchangeName(){
	   return env.getProperty("spe.exchangeName", defaultExchange);
   }

    @Bean
    Queue queue() {
        return new Queue(queueName(), false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(exchangeName());
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(queueName());
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
            MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName());
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    Receiver receiver() {
        return new Receiver();
    }

    @Bean
    MessageListenerAdapter listenerAdapter(Receiver receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }

}
